package com.oreillyauto.service;

import com.oreillyauto.domain.Carpart;

public interface CarpartsService {
	public Carpart getCarpartByPartNumber(String partNumber);
	public Carpart getCarpart(String partNumber) throws Exception;
	public boolean saveCarpart(Carpart carpart);
	public void deleteCarpartByPartNumber(Carpart carpart);
}
