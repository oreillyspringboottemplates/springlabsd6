package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springlabsd6Application {

	public static void main(String[] args) {
		SpringApplication.run(Springlabsd6Application.class, args);
	}

}
