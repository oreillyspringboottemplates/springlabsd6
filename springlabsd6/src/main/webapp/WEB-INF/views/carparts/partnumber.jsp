<%@ include file="/WEB-INF/layouts/include.jsp"%>

<h1>Add New Car Part</h1>
<div class="row">
	<div class="col-sm-12">
		<div>${message}</div>
		<form method="post" action="<c:url value='/carparts/partnumber' />">
			<div class="col-sm-4 form-group">
				<%-- VERSION 1 --%>
<!-- 				<label for="partnumber">Part #</label>
				<orly-input id="partnumber" name="partnumber" placeholder="Part #"></orly-input> -->
				
				<%-- VERSION 2 --%>
				<label for="partnumber">Part#</label>
				<input type="text" id="partnumber" name="partnumber" value="${carpart.partNumber}"  />
			</div>
			<div class="col-sm-4 form-group">
				<%-- Version 1 --%>
<!-- 				<label for="title">Title</label>
				<orly-input id="title" name="title" placeholder="Title"></orly-input> -->
				
				<%-- Version 2 --%>
				<%-- <label for="title">Title</label>
				<input type="text" id="title" name="title" value="${carpart.title}" /> --%>
				
				<%-- Version 2 (Hidden input field) --%>
<%-- 				<c:if test="${not empty carpart}">
    				<input type="hidden" id="update" name="update" value="true" />
				</c:if> --%>
			</div>
			<div class="col-sm-4 form-group">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
		</form>

		<%-- VERSION 4 --%>
<%-- 		<c:if test="${not empty carpart}">
		    <b>Part Number:</b> ${carpart.partNumber}<br/>
		    <b>Title:</b> ${carpart.title}<br/>
		    <a href="<c:url value='/carparts/delete/${carpart.partNumber}'/>" class="btn btn-danger">Delete</a>
		</c:if> --%>

	</div>
</div>
<script>
	orly.ready.then(()=>{
		let message = "${message}";
		let type = "info";
		
		if (message.length > 0) {
			type = ("${messageType}".length > 0) ? "${messageType}" : type;
			orly.qid("alerts").createAlert({type:type, duration:"3000", msg:message});
		}
	});
</script>

<%--
	VERSION 3
 --%>
<c:if test="${not empty carpart}">
    <h3>New Carpart Saved</h3>
    <b>Part Number:</b> ${carpart.partNumber}<br/>
    <b>Title:</b> ${carpart.title} 
</c:if>

 


